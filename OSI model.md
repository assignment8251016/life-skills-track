# OSI Model

In this new age, there are so many devices each with their own proprietary hardware and software that could be completely different from each other. To be able to interconnect different devices to the internet, they need to follow a universal standard necessary that all devices can understand. So, in 1984, the International Organization for Standardization (ISO) released the OSI framework to standardize network design and equipment manufacturing principles. This allows devices that follow OSI model to communicate over the internet with different types of devices.

The Open System Interconnection (OSI) is a standard model that describes the flow of information from one device to another over the internet. The model defines a set of rules for effective data communication between different types of devices. The model is made up of seven layers where each layer performs tasks that are independent of the other layers. These layers are:

1. Physical Layer
2. Data Link Layer
3. Network Layer
4. Transport Layer
5. Session Layer
6. Presentation Layer
7. Application Layer

The layers, arranged in a hierarchical manner, operate in a bottom-to-top fashion, collectively enabling seamless communication.

## Application Layer
The highest layer in the OSI model, the Application Layer provides services and interfaces for applications to access the network. It encompasses various application-specific protocols, such as Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP), and Simple Mail Transfer Protocol (SMTP). This layer enables applications to utilize network resources and communicate with other devices. The main objective of the Application Layer is to facilitate communication between different software applications, regardless of the underlying network infrastructure. It enables applications to establish connections, exchange data, and interpret received data in a format that is meaningful to the user.

## Presentation Layer
The Presentation Layer is the sixth layer in the OSI model. It is responsible for the formatting, translation, and encryption of data to ensure that information sent by the Application Layer can be properly interpreted by the receiving device or application. This layer focuses on the representation and syntax of data exchanged between different systems, abstracting the complexities of data formats and providing a standardized means of communication. The Presentation Layer ensures that data sent by the Application Layer can be effectively transmitted and understood by the receiving device or application. It abstracts the complexities of data formats and enables interoperability between systems with varying data representations. Additionally, the Presentation Layer plays a crucial role in data security by encrypting sensitive information, protecting it from unauthorized access and tampering.

## Session Layer
The Session Layer is the fifth layer in the OSI model. It focuses on establishing, managing, and terminating communication sessions or connections between applications running on different devices. The Session Layer ensures that reliable and synchronized communication is maintained throughout the duration of a session, allowing applications to exchange data in an organized and coordinated manner. By providing these session-related services, the Session Layer enables applications to establish and maintain organized communication sessions. It ensures reliable and coordinated data exchange, allowing applications to work together. It provides services like:

1. Session Establishment
2. Termination 
3. Session Management 
4. Session Synchronization 
5. Dialog Control
6. Token Management

## Transport Layer
The Transport Layer is the fourth layer in the OSI model. It plays a critical role in ensuring reliable and efficient end-to-end delivery of data between devices on a network. This layer is responsible for breaking down large chunks of data from the upper layers into smaller segments, managing their transmission, and reassembling them at the receiving end. The Transport Layer acts as an intermediary between the upper-layer application protocols and the underlying network infrastructure. It provides reliable, ordered, and efficient data transfer services while considering factors such as data segmentation, connection management, flow control, error detection and recovery, congestion control, and QoS requirements. The protocols at this layer, such as TCP and UDP, enable end-to-end communication and ensure the successful delivery of data across networks.The Transport Layer performs six main functions:

1. Segmentation and Reassembly
2. Connection Establishment and Termination
3. Flow Control
4. Error Detection and Recovery
5. Congestion Control
6. Quality of Service (QoS)

## Network Layer
TThe Network Layer is the third layer in the OSI model. It focuses on the routing and forwarding of data packets across different networks to reach their intended destinations. The primary function of the Network Layer is to establish end-to-end logical connections between devices and ensure efficient and reliable data transmission.The Network Layer interacts closely with the Data Link Layer below it and the Transport Layer above it. It receives data from the Transport Layer, encapsulates it into packets, and passes them to the Data Link Layer for transmission over the physical network. At the receiving end, it removes the network-layer headers, reassembles the packets, and delivers the data to the Transport Layer.It abstracts the complexities of network infrastructure, allowing devices to communicate seamlessly while hiding the underlying network topology. The key functions of network layer are

1. Logical Addressing
2. Routing
3. Packet Forwarding
4. Packet Fragmentation and Reassembly
5. Logical Subnetting
6. Quality of Service (QoS)

## Data Link Layer
The Data Link Layer is the second layer in the OSI model. It primarily focuses on the reliable and error-free transmission of data frames between adjacent network nodes, such as between a source and destination device connected by a physical link. Its main responsibility is to provide a dependable communication link over the physical layer. The Data Link Layer operates at the link level and interacts with the Physical Layer below it and the Network Layer above it. It receives data frames from the Network Layer, encapsulates them with header and trailer information, and transmits them over the physical medium. At the receiving end, it removes the header and trailer, performs error checking, and delivers the data frames to the Network Layer. The key functions of data link layer are:

1. Frame Delimiting
2. Physical Addressing
3. Error Detection and Correction
4. Flow Control
5. Access Control
6. Media Access Control (MAC)

## The Physical Layer

It is the lowest layer in the OSI model. It deals with the physical transmission of raw unstructured data bits over the physical medium, such as cables, wireless signals, or optical fibers. It focuses on the electrical, mechanical, and functional characteristics of the physical interface and medium.The Physical Layer operates at the physical level, directly interacting with the physical medium and the electrical or optical signals. It receives raw data bits from the Data Link Layer above it and converts them into physical signals for transmission. At the receiving end, it receives the physical signals, converts them back into data bits, and passes them to the Data Link Layer.The Physical Layer performs the following key functions:

1. Physical Signaling
2. Physical Medium
3. Physical Topology
4. Transmission Mode
5. Bit Synchronization
6. Physical Addressing

