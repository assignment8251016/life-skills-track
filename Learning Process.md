## Question 1

Take a peice of paper write the concept at the top and try to explain it in your own words that is easy to understand. Work through some examples to check if your are lagging somewhere. This is will help identify area of concept that you need to work on. Once that is identified go back to notes to understand it again. Pinpoint complex terms of the concept and try to simplify it as much as you can, that you are able to explain it to someone who does not have the base knowledege needed to understand your concept. 

## Question 2
I will write the concept that I am learning on paper just using simple words. Then i will try to explain it to my colleague and if I get stuck somewhere then i will go back to understand the concept again. 

## Question 3
If you are stuck on some idea that requires high concentration and you are not able to  progress. Then just relax and think about it lightly. As soon as your mind gives you something related to that, start thinking about it with full concentration. Keep doing this until you have not understood.


## Question 4
I would never stress on anything that I find really dificult. I would just relax, go about it slowly with light concentration allowing my mind sometime to figure out something.

## Question 5
Whatever I have learned I will practice it so much that next time when I need it, I don't have to think it is just there.

## Question 6
I will find a good book or video that explains the topic in simple words and watch or read about the topic repeatedly unless I have understood it. If I am still not getting it then I will take some rest and let my unconscious mind work on it. 
