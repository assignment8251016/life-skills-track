### What kinds of behaviour cause sexual harassment?

1. Making conditions of employment or advancement dependent on sexual favors, either explicitly or implicitly.
2. Physical acts of sexual assault.
3. Requests for sexual favors also know as "Quid pro quo harassment".
4. Verbal harassment of a sexual nature, including jokes referring to sexual acts or sexual orientation.
5. Unwanted touching or physical contact.
6. Unwelcome sexual advances.
7. Discussing sexual relations/stories/fantasies at work, school, or in other inappropriate places.
8. Feeling pressured to engage with someone sexually.
9. Exposing oneself or performing sexual acts on oneself.
10. Unwanted sexually explicit photos, emails, or text messages.

### What would you do in case you face or witness any incident or repeated incidents of such behaviour?
1. Do whatever is necessary to stop the harassment immediately.
2. Reach out to someone you trust, such as a supervisor, manager, human resources representative, or a coworker who can offer guidance and support. Share your concerns and provide them with the documented information.
3. Follow the appropriate channels to report the incidents. This may involve submitting a formal complaint to your supervisor, human resources department, or designated authority within your organization. Provide them with the documented information and any supporting evidence you have.
4. If your organization does not adequately address the issue or if you face retaliation for reporting, you may consider seeking external support. This can involve consulting with an employment lawyer, contacting a relevant regulatory agency, or filing a complaint with appropriate authorities.
