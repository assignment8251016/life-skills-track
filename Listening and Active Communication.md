### 1. What are the steps/strategies to do Active Listening? 
1. Resist the temptation to let my own thoughts wander. Instead, consciously focus on the speaker and the topic at hand. By staying mentally engaged, I can better understand their message.

2.  Avoid interrupting the speaker, allowing them to express themselves fully before responding. This shows respect and gives them the opportunity to articulate their thoughts completely.

3. I will Maintain eye contact, nod in agreement or understanding, and use facial expressions that reflect my attentiveness. These gestures reassure the speaker that I am fully engaged.

4. In important conversations, if appropriate, I will consider taking notes. Jotting down key points or ideas can help me remember important details later on. However, I will be mindful not to let my note-taking distract me from actively listening.

5. To ensure understanding, rephrase what the speaker has said in my own words. It shows that I are making an effort to align my understanding with theirs.

### 2. According to Fisher's model, what are the key points of Reflective Listening?

According to Fisher's model, Reflective Listening involves focusing on key points. These key points include:

1. Reflective Listening aims to grasp the speaker's emotions and feelings. It involves acknowledging and empathizing with their emotional state.

2. A crucial aspect of Reflective Listening is paraphrasing, which means restating or summarizing the speaker's words in my own language. This helps demonstrate that you understand what they are saying.

3. Reflective Listening emphasizes withholding judgment and refraining from jumping to conclusions. It involves creating a safe and non-judgmental space for the speaker to express themselves openly.

4. Reflective Listening requires active engagement, where the listener fully immerses themselves in the conversation. They actively participate by responding appropriately and showing interest.

5. To ensure accurate understanding, Reflective Listening involves seeking clarification when something is unclear. Asking questions and seeking further explanation helps prevent misinterpretation.

### 3. what are the obstacles in your listening process?

1. External distractions such as noise, interruptions, or a chaotic environment easily takes my attention away from the speaker and make it challenging to focus on what they are saying.

2. Pre-existing beliefs, judgments, or biases cloud my ability to listen objectively. 

3. My wandering mind  prevents me from fully absorbing the speaker's words and understanding their message.

4. Information overload: When there is an excessive amount of information to process, it overwhelms my capacity to listen effectively. I miss important details when I try to absorb too much at once.

### 4. What can you do to improve your listening?
1. I try to give full attention to the speaker, minimize distractions, both external and internal.I try Engage in active listening by focusing on the speaker's words and non-verbal cues.

2. I wouold let the speaker to express himslef fully without interrupting. By being patiet and resist my urge to interject. Give them space to share their thoughts before responding.

3. I will ask for feedback from others about my listening skills. This can provide valuable insights into areas where I can improve and help me gauge the effectiveness of my listening efforts.

### 5. When do you switch to Passive communication style in your day to day life?

1. When dealing with aggressive or hostile individuals, employing a passive communication style can help defuse tension and prevent further conflict.

2. If I sense that someone prefers not to engage in a conversation or share personal information, adopting a passive communication style shows respect for their boundaries.

3. In certain cultures a more passive communication style is the accepted norm and is regarded as a sign of respect.

4. When receiving feedback or criticism, using a passive communication style allows you to listen openly and attentively without becoming defensive.

### 6. When do you switch into Aggressive communication styles in your day to day life?
1. If someone consistently violates my boundaries or disrespects my rights, assertive communication can be used to firmly establish and enforce my boundaries. This can help you assert my needs and stand up for myself.
2.  where I need to defend the  interests of someone who is unable to assert themselves, assertive communication can be employed.

### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

1. I feel uncomfortable with direct confrontation. Instead of addressing the issue directly, I use sarcasm and indirect remarks to express my dissatisfaction.
2. In situations where I fear consequences, I may use passive-aggressive communication to express my unhappiness without directly confronting those in higher positions.

### 8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?
1. Clearly express my needs using direct and concise sentance. i will Avoid vagues statements that may lead to misunderstandings.
2. I will maintain eye contact, and use assertive body language to convey confidence. I will speak with a steady tone of voice.
3. Clearly define my personal boundaries and communicate them assertively. Let others know what is acceptable and unacceptable to me, and be willing to negotiate and compromise when necessary.
4. When I am faced with conflicts or challenges, I approach them assertively by focusing on finding mutually beneficial solutions.
