
# Q1. What are the activities you do that make you relax - Calm quadrant?

- Engaging in a brief meditation session has allowed me to relax and declutter my mind.
-  I typically go for a walk to loosen up and release the numerous thoughts that occupy my mind at night.
- Having meaningful conversations with my loved ones has proven to be an effective way to reduce my stress levels and concentrate on more pressing matters. 
-  Additionally, listening to my preferred genre of music has a calming effect on my thoughts.

# Q2. When do you find getting into the Stress quadrant?
- When I am unable to solve some problem.
- When I see someone doing anything improperly, I get upset
- I become worried when I have a lot of things to complete and a short deadline.
- I become nervous when I can't perform a task perfectly.

# Q3. How do you understand if you are in the Excitement quadrant?

- Completing projects on time brings me happiness. 
- I find unexpected visits from people to be enjoyable. 
- Spending time on vacations with my loved ones and friends is something I truly cherish.

# Q4. Paraphrase the Sleep is your Superpower video in detail.
- Sleep is a crucial component of the learning process, both before and after acquiring new knowledge.
- Prior to learning, adequate sleep prepares our brain to process information more efficiently.
- Research conducted by the speaker indicates that individuals who obtain sufficient sleep every night are 40% more productive when learning
- In contrast, limiting sleep to only 4 hours can lead to a 75% reduction in natural killer cells, which play a critical role in our body's defense against disease.
- Establishing a consistent sleep schedule can improve the quality of our sleep. Additionally, maintaining low body and room temperatures can facilitate rapid sleep onset.

# Q5. What are some ideas that you can implement to sleep better?

- To improve my overall health and well-being, I have implemented several positive habits.
- These include reducing my caffeine intake, adhering to a consistent prayer and meditation practice, limiting late-night meals, and avoiding phone usage prior to bedtime.
- Additionally, establishing a regular sleep schedule has been instrumental in achieving restful and rejuvenating sleep.

# Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- During a solo kayaking trip, the speaker realized that she was the least skilled member of the group she had joined. 
- This realization led to a commitment to improve her behavior and overall health.
- To achieve her health goals, the speaker began exercising regularly, trying various forms of physical activity such as kickboxing, yoga, and Zumba.
- Scientific literature supports that exercise can improve mood, energy levels, memory, and focus by strengthening the prefrontal cortex and hippocampus.
- The speaker recommends exercising for at least 30 minutes, four times per week, and mentions that power walking can be a convenient form of exercise that does not require a gym membership.

# Q7. What are some steps you can take to exercise more?

- I commit to increasing my physical activity and completing my daily tasks independently, without assistance.
- It is recommended to incorporate at least 15 to 20 minutes of physical activity daily.
- Opting to walk short distances rather than relying on transportation is a good way to achieve this goal. 
- Exercising with a friend can provide motivation and enhance the enjoyment of the workout experience.
